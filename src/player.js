import Bullet from './bullet'
import router from './router'
export default class Player {
  constructor (name, x, y, enermy = false) {
    this.name = name
    this.position = {}
    this.position.x = x
    this.position.y = y
    this.speed = 1
    this.direct = 0 //  方向0：朝上，1朝左，2朝下，3朝右
    this.attack = 1 // 攻击力
    this.flood = 5 // 血量
    this.sock = null // socket连接
    this.state = 0 // 0:停止，1：移动
    this.bullets = []
    this.enermys = []
    if (!enermy) {
      this.init()
    }
  }
  init () {
    this.sock = new WebSocket('ws://localhost:8082')
    this.sock.onopen = () => { this.onSockOpen() }
    this.sock.onmessage = (data) => { this.onSockMessage(data) }
    this.sock.onclose = () => { this.onSockClose() }
  }
  send (data) {
    if (this.sock) {
      this.sock.send(JSON.stringify(data))
    }
  }
  hurt (player) {
    player.flood--
    if (player.flood === 0) {
      this.send({ opt: 'kill', killer: this.name, killed: player.name })
    } else {
      this.send({ opt: 'upblood', name: player.name, blood: player.flood })
    }
  }
  shoot () {
    let bullet = new Bullet(this.enermys, this, this.direct, this.position.x, this.position.y, this.speed)
    this.bullets.push(bullet)
    console.log(this.bullets.length)
    this.send({ opt: 'shoot', name: this.name })
  }
  setName (username) {
    this.name = username
    // 上报自己的位置
    this.send({ opt: 'init', name: this.name, x: this.position.x, y: this.position.y })
  }
  setMessageBox (messagebox) {
    this.messagebox = messagebox
  }
  setRouter (router) {
    this.router = router
  }
  setEnermy (enermy) {
    this.enermy = enermy
  }
  onSockOpen () {
  }
  onSockMessage (e) {
    let data = JSON.parse(e.data)
    switch (data.result) {
      case 'error':
        this.messagebox.add({ sender: '系统消息', message: data.message })
        break
      case 'talk': // 聊天信息
        this.messagebox.add({ sender: data.sender, message: data.message })
        break
      case 'initok': // 初始化成功
        let playerList = data.playerList
        playerList.forEach((enermy, key, owner) => {
          if (enermy.name !== this.name) {
            let ee = new Player(enermy.name, enermy.position.x, enermy.position.y, true)
            this.enermy.set(enermy.name, ee)
            this.enermys.push(ee)
          }
        })
        router.push({ path: 'game' })
        break
      case 'upposition': // 更新其他玩家信息
        let enermy = this.enermy.get(data.name)
        enermy.direct = data.direct
        enermy.position.x = data.position.x
        enermy.position.y = data.position.y
        console.log(enermy.direct)
        break
      case 'upblood':
        let enermy1 = this.enermy.get(data.name)
        enermy1.flood = data.blood
        break
      case 'shoot': // 玩家射击
        let enermy2 = this.enermy.get(data.name)
        enermy2.shoot()
        break
      case 'kill': // 玩家死亡
        let enermy3 = this.enermy.get(data.killed)
        this.messagebox.add({ sender: data.kill, message: '杀死' + enermy3.name })
        this.enermy.delete(enermy3.name)
        this.enermys.splice(this.enermys.findIndex(enermy3), 1)
        break
    }
  }
  onSockClose () {
  }
  onframe () {
    if (this.state === 1) {
      this.move(this.direct)
      this.send({ opt: 'upposition', name: this.name, x: this.position.x, y: this.position.y, direct: this.direct })
    }
  }
  move (direct) {
    this.direct = direct
    switch (direct) {
      case 0:
        if (this.position.y > 0) {
          this.position.y -= this.speed
        }
        break
      case 1:
        if (this.position.x > 0) {
          this.position.x -= this.speed
        }
        break
      case 2:
        if (this.position.y < window.innerHeight - 55) {
          this.position.y += this.speed
        }
        break
      case 3:
        if (this.position.x < window.innerWidth - 55) {
          this.position.x += this.speed
        }
        break
    }
  }
}
