export default class Bullet {
  constructor (players, player, direct, x, y, speed) {
    this.players = players
    this.player = player
    this.direct = direct
    this.speed = speed
    this.isdie = false
    this.position = {}
    this.position.x = x + 22
    this.position.y = y + 22
  }
  move () {
    switch (this.direct) {
      case 0:
        this.position.y -= this.speed
        break
      case 1:
        this.position.x -= this.speed
        break
      case 2:
        this.position.y += this.speed
        break
      case 3:
        this.position.x += this.speed
        break
    }
    // 判断是否超出场景
    if (this.position.x <= 0 || this.position.x >= window.innerWidth || this.position.y <= 0 || this.position.y >= window.innerHeight) {
      this.isdie = true
      this.player.bullets.splice(this.player.bullets.findIndex(n => n === this), 1)
    }
    // 判断是否击中敌人
    for (let player of this.players) {
      if (player === this.player) {
        continue
      }
      if (this.position.x <= player.position.x + 22 && this.position.x >= player.position.x - 22 && this.position.y <= player.position.y + 22 && this.position.y >= player.position.y - 22) {
        this.isdie = true
        this.player.hurt(player)
        this.player.bullets.splice(this.player.bullets.findIndex(n => n === this), 1)
      }
    }
  }
  onframe () {
    if (!this.isdie) {
      this.move()
    }
  }
}
