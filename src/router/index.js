import Vue from 'vue'
import Router from 'vue-router'
import Game from '../components/Game.vue'
import Login from '../components/Login.vue'
import Ready from '../components/Ready.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/ready',
      component: Ready
    },
    {
      path: '/game',
      component: Game
    }
  ]
})
